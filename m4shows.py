
#!/usr/bin/python3
"""
Media.4local.ru TV Shows checker

Prints out magnet links to all episodes of added shows
"""

# TODO:
#   * Raise an HTTP server on demand and show links on web

import requests
from bs4 import BeautifulSoup
import re
import sys


class Show:
    """
    show's name, url, season (optional)
    """

    def __init__(self, name, url, season=1):
        self.name = name
        self.url = url
        self.season = season

    def __repr__(self):
        return "\nTitle: {}\nSeason: {}\nUrl: {}\n".format(self.name, self.season, self.url)

    def episodes(self):
        """
        Show links to episodes
        """

        print("\tLinks for \033[92m\033[1m" + self.name +
              " (season {})\033[0m:".format(self.season))

        request = requests.get(self.url).content

        content = str(BeautifulSoup(request, 'lxml').find_all(
            'div', style="text-align:center;"))

        links = re.compile('(magnet\S+.mp4|magnet\S+.avi|magnet\S+.mkv)')

        for i, link in enumerate(re.findall(links, content)):
            print("=> Episode " + str(i + 1) +
                  ": ", link.replace('&amp;', '&'))

        endline = "\n" + "-" * 78 + "\n"

        print("\n\tOr visit the URL: \033[94m {} \033[0m".format(
            self.url) + endline)

# Test `shows`:
wayward = Show("Wayward Pines", "http://media.4local.ru/serials/foreign-serials/hd-serial/75661-ueyuord-payns-wayward-pines-sezon-2-2016-web-dl-720p.html", season=2)
krugosvetka = Show(
    "Кругосветка", "http://media.4local.ru/documental/journey/74443-orel-i-reshka-krugosvetka-12-sezon-2016-satrip.html", season=12)
wrecked = Show(
    "Wrecked", "http://media.4local.ru/serials/foreign-serials/comedy-f-series/75909-krushenie-wrecked-sezon-1-2016-web-dlrip.html")


wayward.episodes()
krugosvetka.episodes()
wrecked.episodes()

# Object representation:
# print(wayward)

if __name__ == '__main__':
    try:
        added_show = Show(sys.argv[1], sys.argv[2])
        added_show.episodes()
    except IndexError:
        pass

    print('Made by Vladislav Andreev <me@vld.by>')
