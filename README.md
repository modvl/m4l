# Парсер magnet-ссылок на сериалы/кино с media.4local.ru

### Зависимости:
* beautifulsoup, lxml

## Запуск скрипта для сериалов:

* Обычный запуск скрипта для сериалов:

```
#!bash

python3 m4shows.py
```


* Запуск скрипта для сериалов и получение ссылок через передачу параметров:
```
#!bash

python3 m4shows.py TvShowName http://media.4local.ru/some_show_url.html
```

## Запуск скрипта для кино:
```
#!bash

python3 m4films.py category count_of_pages
```
Где `category` - это категория, а `count_of_pages` - число страниц

### Настройка для обычного запуска скрипта для сериалов:

* Добавьте следующий код:

```
#!python

new_show_name = Show("Show Name", "http://media.4local.ru/some_show_url.html", season=2)
# Параметр `season` опциональный, по-умолчанию `season = 1`

new_show_name.episodes() # Получение списка ссылок
```

* Запустите:
```
#!bash

python3 m4l.py
```

*Примерный вывод*:


```
Links for Show Name (season 2):
=> Episode 1:  magnet:?xt=urn:tree:tiger:TSNUSG2HNGOKXXX44QUOT7U4GRY7DZQL6AL4IUQ&amp;xl=236539904&amp;dn=Kie.1.01.ShowName.avi
=> Episode 2:  magnet:?xt=urn:tree:tiger:H3AEFWIZ5K674QXT2SMEPCJ2L6KG66NAUOMMDWI&amp;xl=261904384&amp;dn=Kie.1.02.ShowName.mkv

	Or visit the URL:  http://media.4local.ru/some_show_url.html

```
