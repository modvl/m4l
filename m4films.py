#!/usr/bin/python3
"""
Media.4local.ru film links parser.
"""

import re
import requests
from bs4 import BeautifulSoup
import sys


def films(category, p):

    for page in range(1, p + 1):

        # Building a link
        url = 'http://media.4local.ru/films/{}/page/{}/'.format(category, page)

        r = requests.get(url)
        raw_body = r.text

        pretty_body = BeautifulSoup(raw_body, 'lxml')

        match = re.compile('(http:\/\/media\.4local\.ru\/\S+\.html)')

        for link in pretty_body.find_all('h3', class_='btl'):

            # Looking for links on film pages
            z = re.findall(match, str(link))
            film = requests.get(z[0]).content

            # Looking for magnets
            content = str(BeautifulSoup(film, 'lxml').find_all(
                'div', style="text-align:center;"))
            links = re.compile('(magnet\S+.mp4|magnet\S+.avi|magnet\S+.mkv)')

            print(re.findall(links, content)[0].replace('&amp;', '&'))


if __name__ == '__main__':
    try:
        films(category=sys.argv[1], p=int(sys.argv[2]))
    except IndexError:
        print('\tThere were too many or too few arguments!')
        print(sys.argv)
        print('\tYou should\'ve passed two: category count_of_pages_to_search')
